package com.example.chatlogin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

import androidx.annotation.Nullable;

public class LocalDB extends SQLiteOpenHelper {
    public LocalDB(@Nullable Context context) {
        super(context, "chatapplocaldb", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table contact (mycontact text)");
        sqLiteDatabase.execSQL("create table chats(this_user text,friend_name text,message text,date date,time date,type bool)"); //0 is send 1 is received
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists chats");
        sqLiteDatabase.execSQL("drop table if exists contact");
    }
    public boolean insertContact(String username){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        ContentValues contentValues=new ContentValues(); // put data im db
        contentValues.put("mycontact",username);
        Long result=sqLiteDatabase.insert("contact",null,contentValues);
        if (result==-1)
            return false;
        return true;
    }
    public void deleteAll(){ //delete all contacts from db
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("delete from contact");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public Cursor getContact(){ // get contact from db
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("select * from contact",null);
        return cursor;
    }
    public boolean insertToChat(String this_user, String friendname, String message, String date,String time,boolean type){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("this_user",this_user);
        contentValues.put("friend_name",friendname);
        contentValues.put("message",message);
        contentValues.put("date",date);
        contentValues.put("time",time);
        contentValues.put("type",type);
        Long result=sqLiteDatabase.insert("chats",null,contentValues);
        if(result==-1)
            return false;
        return true;

    }
    public Cursor getChatsforUser(String this_user,String friendname){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("select message,time,type,date from chats where friend_name=? and this_user=? order by date", new String[]{friendname,this_user});
        return cursor;
    }
    public Cursor getLeatestMessage (String this_user){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("select friend_name,message from chats where this_user=? group by friend_name having max(date) order by date desc", new String[]{this_user});
        return cursor;
    }
    public void deleteContact(String contact){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("delete from contact where mycontact='"+contact+"'");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public void deleteMessge(Message temp,String this_user,String friend_name){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("delete from chats where this_user='"+this_user+"' and friend_name='"+friend_name+"' and message='"+temp.message+"' and date like '%"+temp.time+"%'");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public void deleteMessagesBetweenFriends(String friend_name,String this_user){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("delete from chats where friend_name='"+friend_name+"' and this_user='"+this_user+"' ");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
//    public boolean blockContact(String contactName){
//        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
//        ContentValues contentValues=new ContentValues();
//        contentValues.put();
//    }
}
