package com.example.chatlogin;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RetrofitInterface {

    @POST("mobilelogin")
    Call<Void> excuteLogin (@Body HashMap<String,String> map);

    @POST("mobileregister")
    Call<Void> excuteSignup (@Body HashMap<String,String> map);
}
