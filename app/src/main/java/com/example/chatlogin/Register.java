package com.example.chatlogin;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class Register extends AppCompatActivity {
    EditText username;
    EditText password;
    EditText repassword;
    EditText email;
    Button butregister;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
//    String BASE_URL ="http://172.17.100.2:3000";
    String BASE_URL ="http://192.168.43.69:3000";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        username=(EditText)findViewById(R.id.username2);
        password=(EditText)findViewById(R.id.password2);
        repassword=(EditText)findViewById(R.id.repassword);
        email=(EditText)findViewById(R.id.email);
        butregister=(Button)findViewById(R.id.registerbutton);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
    }
    public void registeruser(View v){
        HashMap<String,String> map =new HashMap<>();
        if (username.getText().toString().equals("")) {
            Toast.makeText(Register.this, "Please Enter Username", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length()<8){
            Toast.makeText(Register.this,"Please Use 8 characters or more for your password ",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!password.getText().toString().equals(repassword.getText().toString())){
            Toast.makeText(Register.this,"Those passwords didn’t match. Try again ",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!email.getText().toString().contains("@")){
            Toast.makeText(Register.this,"Please Enter Valid Email ",Toast.LENGTH_SHORT).show();
            return;
        }
        map.put("username",username.getText().toString());
        map.put("password",password.getText().toString());
        map.put("email",email.getText().toString());
        Call<Void> call=retrofitInterface.excuteSignup(map);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code()==200){
                    Intent in=new Intent(Register.this,MainActivity.class);
                    editor.putBoolean("Login",true).commit();
                    editor.putString("username",username.getText().toString()).commit();
                    OnlineService.socket.emit("logein",username.getText().toString());
                    startActivity(in);
                    finish();
                }
                else if (response.code()==400)
                    Toast.makeText(Register.this,"sorry, user is exist",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(Register.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }

    public void back(View v){
        Intent in=new Intent(Register.this,Welcome.class);
        startActivity(in);
    }
}