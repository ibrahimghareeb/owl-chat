package com.example.chatlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import static com.example.chatlogin.ContactFragment.DB;

public class SplashScreen extends AppCompatActivity { // appear 2 sec and goes
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        startService(new Intent(getBaseContext(),OnlineService.class));
        Handler handler=new Handler();
        sharedPreferences=getSharedPreferences("myuser", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("Login",false))
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    OnlineService.socket.emit("logein",sharedPreferences.getString("username","hi"));
                    Intent in=new Intent(SplashScreen.this,MainActivity.class);
                    startActivity(in);
                    finish();
                }
            },1000);
        else
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent in=new Intent(SplashScreen.this,Welcome.class);
                    startActivity(in);
                    finish();
                }
            },1000);
        
    }

}