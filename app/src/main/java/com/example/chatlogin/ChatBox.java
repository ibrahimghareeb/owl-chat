package com.example.chatlogin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.socket.emitter.Emitter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.chatlogin.Adapters.MessageAdapter;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class ChatBox extends AppCompatActivity implements MessageAdapter.LongClickOnMessage {
    RecyclerView rv;
    List<Message> itemData;
    MessageAdapter messageAdapter;
    EditText inputmsg;
    Button butsend;
    ImageButton pick_image;
    String name;
    String friendname;
    ActionBar actionBar;
    SharedPreferences sharedPreferences;
    LocalDB DB;
    final int IMAGE_REQUEST_ID=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_box);
        actionBar=getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#253A4B")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        pick_image=(ImageButton)findViewById(R.id.pick_image);
        rv = (RecyclerView) findViewById(R.id.messagshowebox);
        inputmsg = (EditText) findViewById(R.id.inputmsg);
        butsend = (Button) findViewById(R.id.send);
        Intent in = getIntent();
        friendname=in.getStringExtra("friendname");
        actionBar.setTitle(friendname);

        sharedPreferences=getSharedPreferences("myuser", Context.MODE_PRIVATE);
        name=sharedPreferences.getString("username","");
        itemData = new ArrayList<>();
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(true);
        rv.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(itemData,this);
        rv.setAdapter(messageAdapter);
        rv.setItemAnimator(new DefaultItemAnimator());
        DB=new LocalDB(this);
        showMessage();
        OnlineService.socket.on("message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (this.equals(null))
                            System.out.println("null value \n");
                        if(friendname.equals(args[0].toString())){
                            printMessgae(args[0].toString(), args[1].toString(),false,args[2].toString().substring(11,16));
                        }
                        DB.insertToChat(name,friendname,args[1].toString(), args[2].toString(),args[3].toString(),false);
                    }
                });
            }
        });
    }


    public void send(View v){
        if (inputmsg.getText().toString().equals(""))
            return;
        Date currentTime= Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss a");
        // you can get seconds by adding  "...:ss" to it
        dateFormat.setTimeZone(TimeZone.getDefault());
        String time = dateFormat.format(currentTime);
        dateFormat= new SimpleDateFormat("yyyy/MM/dd");
        Date currentdate=Calendar.getInstance().getTime();
        String date=dateFormat.format(currentdate);
        OnlineService.socket.emit("chat",name,friendname,inputmsg.getText().toString(),date+" "+time,time);
        printMessgae(name,inputmsg.getText().toString(),true,time);
        DB.insertToChat(name,friendname,inputmsg.getText().toString(),date+" "+time,time,true);
        inputmsg.setText("");
    }

    public void showMessage(){
        Cursor cursor=DB.getChatsforUser(name,friendname);
        itemData.clear();
        while (cursor.moveToNext()){
            boolean is_sender = cursor.getInt(2) > 0;
            itemData.add(new Message(cursor.getString(0),is_sender,cursor.getString(1)));
        }
        messageAdapter.updateAdapter(itemData);
    }

    void printMessgae(String username,String message,boolean is_sender,String time){
        Message newdata = new Message(message+"  ",is_sender,time);
        itemData.add(newdata);
        messageAdapter = new MessageAdapter(itemData,this);
        messageAdapter.notifyDataSetChanged();
        rv.setAdapter(messageAdapter);
    }
    @Override
    public void onLongClick(int position) {
        AlertDialog.Builder popup=new AlertDialog.Builder(ChatBox.this);
        popup.setCancelable(true);
        popup.setItems(R.array.alert_dialog_for_message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        Message temp=new Message(messageAdapter.getMessage(position),messageAdapter.get_is_sender(position),messageAdapter.getMessageTime(position));
                        DB.deleteMessge(temp,sharedPreferences.getString("username",""),friendname);
                        showMessage();
                }
            }
        });
        popup.show();
    }
    public void pickImage(View v){
        Intent in=new Intent(Intent.ACTION_GET_CONTENT);
        in.setType("image/*");
        startActivityForResult(Intent.createChooser(in,"choose Image"),IMAGE_REQUEST_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //Toast.makeText(ChatBox.this,requestCode+" "+resultCode,Toast.LENGTH_LONG).show();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_ID && resultCode == RESULT_OK) {
            try {
                InputStream is = getContentResolver().openInputStream(data.getData());
                Bitmap img = BitmapFactory.decodeStream(is);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                img.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                String base64 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                OnlineService.socket.emit("sendimage", base64);
                Toast.makeText(ChatBox.this, base64, Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                Toast.makeText(ChatBox.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
    public void printImage(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chatbox_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent in=new Intent(ChatBox.this,MainActivity.class);
                startActivity(in);
                this.finish();
                return true;
            case R.id.unfriend:
                AlertDialog.Builder builder=new AlertDialog.Builder(ChatBox.this);
                builder.setCancelable(true);
                builder.setTitle("Unfriend");
                builder.setMessage("Are you sure you want to remove "+friendname);
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        OnlineService.socket.emit("unfriend",sharedPreferences.getString("username",""),friendname);
                        DB.deleteContact(friendname);
                        DB.deleteMessagesBetweenFriends(friendname,name);
                        Intent in=new Intent(ChatBox.this,MainActivity.class);
                        startActivity(in);
                        finish();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Intent in=new Intent(ChatBox.this,MainActivity.class);
            startActivity(in);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}