package com.example.chatlogin;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import androidx.annotation.Nullable;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class OnlineService extends Service {
    public static Socket socket;
    LocalDB DB;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public int onStartCommand(Intent intent, int flags, int startId){
        DB=new LocalDB(this);
        DB.deleteAll();
        connect();
        socket.on("yourfriends", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONArray ja;
                    DB.deleteAll();
                    try {
                        ja=new JSONArray(args[0].toString());
                        for (int i=0;i<ja.length();i++){
                            JSONObject temp= ja.getJSONObject(i);
                            DB.insertContact(temp.getString("friend_one"));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        socket.on("get_undeleverd_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray ja;
                try {
                    ja=new JSONArray(args[0].toString());
                    for(int i=0;i<ja.length();i++){
                        JSONObject temp=ja.getJSONObject(i);
                        DB.insertToChat(temp.getString("reciver"),temp.getString("sender"),temp.getString("message"),temp.getString("date_of_message"),temp.getString("time_of_message"),false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return START_STICKY;
    }
    public static boolean connect(){
        try {
//            socket = IO.socket("http://172.17.100.2:3000");
//            socket = IO.socket("http://172.16.48.15:3000");
            socket = IO.socket("http://192.168.43.69:3000");
            socket.connect();
            return true;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return false;
    }

}

