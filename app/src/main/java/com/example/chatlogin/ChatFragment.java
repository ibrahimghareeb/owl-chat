package com.example.chatlogin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chatlogin.Adapters.ChatsAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ChatFragment extends Fragment implements ChatsAdapter.ItemListener{
    RecyclerView recyclerViewForChat;
    static List<Chats> chatsList;
    static ChatsAdapter chatsAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    SharedPreferences sharedPreferences;
    LocalDB DB;
    Date currentTime;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.chat_fragment,container,false);
        recyclerViewForChat=(RecyclerView)view.findViewById(R.id.chat_list);
        DB=new LocalDB(view.getContext());
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        chatsList=new ArrayList<>();
        swipeRefreshLayout=view.findViewById(R.id.swipToRefreshChats);
        chatsAdapter=new ChatsAdapter(chatsList,this);
        recyclerViewForChat.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewForChat.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForChat.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        recyclerViewForChat.setAdapter(chatsAdapter);
        showMessage();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showMessage();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }


    public void showMessage(){
        chatsList.clear();
        Cursor cursor=DB.getLeatestMessage(sharedPreferences.getString("username",""));
        while (cursor.moveToNext()){
            chatsList.add(new Chats(cursor.getString(0),cursor.getString(1)));
        }
        chatsAdapter.updateAdapter(chatsList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onClick(int position) {
        Intent in=new Intent(getActivity(),ChatBox.class);
        in.putExtra("friendname",chatsAdapter.getname(position));
        startActivity(in);
        getActivity().finish();
    }
}
