package com.example.chatlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Welcome extends AppCompatActivity {
    Button butlogin;
    TextView butregister;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom);
        butlogin=(Button)findViewById(R.id.login1);
        butregister=(TextView)findViewById(R.id.register);
        if(ContextCompat.checkSelfPermission(Welcome.this, Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(Welcome.this,new String[]{Manifest.permission.INTERNET},1);
    }

    public void login(View v){
        Intent in=new Intent(Welcome.this,Login.class);
        startActivity(in);
        finish();
    }
    public void register(View v){
        Intent in=new Intent(Welcome.this,Register.class);
        startActivity(in);
    }
}