package com.example.chatlogin;

import android.widget.RelativeLayout;

public class Message { // contain data for recyclerview
    public String message;
    public boolean is_sender;
    public String time;

    public Message(String message, boolean is_sender,String time){
        this.message=message;
        this.is_sender=is_sender;
        this.time=time;
    }
}
