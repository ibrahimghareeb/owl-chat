package com.example.chatlogin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chatlogin.Contacts;
import com.example.chatlogin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder>{

    List<Contacts> itemcontact;
    private ItemListener onItemClick;
    public ContactAdapter(List<Contacts>itemcontact, ItemListener onItemClick){
        this.itemcontact=itemcontact;
        this.onItemClick=onItemClick;
    }

    @NonNull
    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.contactdesign,parent,false);
        ContactAdapter.ViewHolder vh=new ContactAdapter.ViewHolder(v,onItemClick);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(itemcontact.get(position).name);

    }
    public String getname(int position){
        return itemcontact.get(position).name;
    }

    @Override
    public int getItemCount() {
        return this.itemcontact.size();
    }

    public void updateAdapter(List<Contacts> itemcontact){
        this.itemcontact=itemcontact;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            textView=(TextView) itemView.findViewById(R.id.names_on_contact_frag);
            this.itemListener=itemListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());
        }
    }
    public interface ItemListener{
        void onClick(int position);
    }

}
