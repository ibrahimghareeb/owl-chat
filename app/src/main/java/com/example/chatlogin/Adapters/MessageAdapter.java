package com.example.chatlogin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chatlogin.Message;
import com.example.chatlogin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder>{

    private List<Message> itemData;
    public static final int SEND_MESSAGE=1;
    public static final int RECIVED_MESSAGE=0;
    LongClickOnMessage longClickOnMessage;
    public MessageAdapter (List<Message> itemData,LongClickOnMessage longClickOnMessage){
        this.itemData=itemData;
        this.longClickOnMessage=longClickOnMessage;
    }
    @NonNull
    @Override

    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==SEND_MESSAGE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sendedmessage, parent, false);
            ViewHolder vh = new ViewHolder(v,longClickOnMessage);
            return vh;
        }
        else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recivedmessage, parent, false);
            ViewHolder vh = new ViewHolder(v,longClickOnMessage);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ViewHolder holder, int position) {
        //holder.msgusername.setText(itemData.get(position).username);
        holder.msg.setText(itemData.get(position).message);
        holder.time.setText(itemData.get(position).time);
    }

    @Override
    public int getItemViewType(int position) {
        if (itemData.get(position).is_sender)
            return SEND_MESSAGE;
        else
            return RECIVED_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public void updateAdapter(List<Message> itemData){
        this.itemData=itemData;
        notifyDataSetChanged();
    }
    public String getMessage(int position){
        return itemData.get(position).message;
    }
    public String getMessageTime(int position){return itemData.get(position).time;}
    public Boolean get_is_sender(int position){return itemData.get(position).is_sender;}


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        //public TextView msgusername;
        public TextView msg;
        public TextView time;
        LongClickOnMessage longClickOnMessage;
        public ViewHolder(@NonNull View itemView,LongClickOnMessage longClickOnMessage) {
            super(itemView);
            //msgusername=(TextView) itemView.findViewById(R.id.msgusername);
            msg=(TextView)itemView.findViewById(R.id.msg);
            time=(TextView)itemView.findViewById(R.id.messagetime);
            this.longClickOnMessage=longClickOnMessage;
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            longClickOnMessage.onLongClick(getAdapterPosition());
            return true;
        }
    }
    public interface LongClickOnMessage{
        public void onLongClick(int position);
    }
}
