package com.example.chatlogin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chatlogin.Chats;
import com.example.chatlogin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    List<Chats> chatsList;
    ItemListener itemListener;
    public ChatsAdapter(List<Chats> chatsList,ItemListener itemListener){
        this.chatsList=chatsList;
        this.itemListener=itemListener;
    }
    @NonNull
    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.chatdesign,parent,false);
        ChatsAdapter.ViewHolder vh=new ChatsAdapter.ViewHolder(v,itemListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.username.setText(chatsList.get(position).friendName);
        holder.last_message.setText(chatsList.get(position).latest_message);
    }
    public String getname(int position){
        return chatsList.get(position).friendName;
    }

    @Override
    public int getItemCount() {
        return chatsList.size();
    }
    public void updateAdapter(List<Chats> chatsList){
        this.chatsList=chatsList;
        notifyDataSetChanged();
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //viewholder to link xml with recyclerview + implements interfaces onclick
        TextView username;
        TextView last_message;

        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            username=(TextView) itemView.findViewById(R.id.names_on_chat_frag);
            last_message=(TextView) itemView.findViewById(R.id.last_message);
            this.itemListener=itemListener;
            itemView.setOnClickListener(this);
        }

        @Override // implements orginal onclick
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());// مشان اعرف ع مين ضغطت من اسماء الchat

        }
    }
    public interface ItemListener{
        void onClick(int position);
    }
}
