package com.example.chatlogin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.chatlogin.FriendsRequestTypeInRV;
import com.example.chatlogin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FriendsRequestTypeAdapter extends RecyclerView.Adapter<FriendsRequestTypeAdapter.ViewHolder> {
    List<FriendsRequestTypeInRV> items;
    public ItemListener onClickListner;
    public FriendsRequestTypeAdapter(List<FriendsRequestTypeInRV> items, ItemListener onClickListner){
        this.items=items;
        this.onClickListner=onClickListner;
    }
    @NonNull
    @Override
    public FriendsRequestTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_request_view,parent,false);
        FriendsRequestTypeAdapter.ViewHolder vh=new FriendsRequestTypeAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsRequestTypeAdapter.ViewHolder holder, int position) {
        holder.friendName.setText(items.get(position).friendName);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    public void updateAdapter(List<FriendsRequestTypeInRV> items){
        this.items=items;
        notifyDataSetChanged();
    }
    public String getname(int position){
        return items.get(position).friendName;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView friendName;
        Button accept;
        Button delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            friendName=(itemView).findViewById(R.id.friend_name);
            accept=(itemView).findViewById(R.id.accept_friend);
            delete=(itemView).findViewById(R.id.delete_request);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onAcceptClick(view,getAdapterPosition());
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onDeleteClick(view,getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }
    public interface ItemListener {
       public void onAcceptClick(View v,int position);
        public void onDeleteClick(View v,int position);
    }
}
