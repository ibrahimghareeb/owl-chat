package com.example.chatlogin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.chatlogin.SearchResult;
import com.example.chatlogin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder>{

    List<SearchResult> listfriend;
    private ItemListener onButtonAddClick;
    SearchResultAdapter.ViewHolder Holder;
    public SearchResultAdapter(List<SearchResult>listfriend, ItemListener onButtonAddClick){
        this.listfriend=listfriend;
        this.onButtonAddClick=onButtonAddClick;
    }
    @NonNull
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.searched_friends_view,parent,false);
        SearchResultAdapter.ViewHolder vh=new SearchResultAdapter.ViewHolder(v,onButtonAddClick);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultAdapter.ViewHolder holder, int position) {
        holder.friendsname.setText(listfriend.get(position).friendName);
        Holder=holder;
    }

    @Override
    public int getItemCount() {
        return listfriend.size();
    }

    public void updateAdapter(List<SearchResult> listfriend){
        this.listfriend=listfriend;
        notifyDataSetChanged();
    }
    public String getname(int position){
        return listfriend.get(position).friendName;
    }

    public void changeButtonTitleToRequested(){
        Holder.add.setText("Requested");
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView friendsname;
        Button add;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            friendsname=itemView.findViewById(R.id.friendname);
            add=itemView.findViewById(R.id.add_friends_button);
            this.itemListener=itemListener;
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonAddClick.onClick(v,getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }
    public interface ItemListener{
        void onClick(View v,int position);
    }
}
