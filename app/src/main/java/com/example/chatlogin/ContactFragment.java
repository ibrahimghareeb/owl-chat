package com.example.chatlogin;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chatlogin.Adapters.ContactAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ContactFragment extends Fragment implements ContactAdapter.ItemListener {
    RecyclerView recyclerView;
    static List<Contacts> contactsList;
    static ContactAdapter contactadapter;
    static LocalDB DB;
    SwipeRefreshLayout swipeRefreshLayout;
    public static boolean activate=false;
    @Override
    public void onStart() {
        super.onStart();
        activate=true;
    }

    @Override
    public void onStop() {
        super.onStop();
        activate=false;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.contact_fragment,container,false);
        recyclerView=view.findViewById(R.id.contactslist);
        contactsList=new ArrayList<>();

        contactadapter=new ContactAdapter(contactsList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(contactadapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.refreshlayout);
        DB=new LocalDB(view.getContext());
        showContact();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showContact();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }



    public static void showContact(){
        Cursor cursor=DB.getContact();
        contactsList.clear();
        while(cursor.moveToNext()) {
            contactsList.add(new Contacts(cursor.getString(0)));
        }
        contactadapter.updateAdapter(contactsList);
    }

    @Override
    public void onClick(int position) {
        Intent in=new Intent(getActivity(),ChatBox.class);
        in.putExtra("friendname",contactadapter.getname(position));
        startActivity(in);
        getActivity().finish();
    }
}
