package com.example.chatlogin;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;

public class Login extends AppCompatActivity {
    EditText username;
    EditText password;
    Button butlogin;
    TextView butregister;
    ImageView butback;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
//    String BASE_URL ="http://172.17.100.2:3000";
//    String BASE_URL ="http://172.16.48.15:3000";
    String BASE_URL ="http://192.168.43.69:3000";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        username=(EditText)findViewById(R.id.username1);
        password=(EditText)findViewById(R.id.password1);
        butlogin=(Button)findViewById(R.id.login2);
        butback=(ImageView)findViewById(R.id.back);
        editor=getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }
    public void login(View v) throws IOException {

        HashMap <String,String> map =new HashMap<>();
        if (username.getText().toString().equals("")) {
            Toast.makeText(Login.this, "Please Enter Username", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.getText().toString().equals("") ){
            Toast.makeText(Login.this,"Please Enter your password ",Toast.LENGTH_SHORT).show();
            return;
        }
        map.put("username",username.getText().toString());
        map.put("password",password.getText().toString());
        Call <Void> call=retrofitInterface.excuteLogin(map);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code()==200) {
                    Intent in=new Intent(Login.this,MainActivity.class);
                    editor.putBoolean("Login",true).commit();
                    editor.putString("username",username.getText().toString()).commit();
                    OnlineService.socket.emit("logein",username.getText().toString());
                    startActivity(in);
                    finish();
                }
                else if (response.code()==404)
                    Toast.makeText(Login.this,"failed",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(Login.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
    public void register(View v){
        Intent in=new Intent(Login.this,Register.class);
        startActivity(in);
    }
    public void back(View v){
        Intent in=new Intent(Login.this,Welcome.class);
        startActivity(in);
    }
}