package com.example.chatlogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.socket.emitter.Emitter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatlogin.Adapters.SearchResultAdapter;
import com.example.chatlogin.Adapters.FriendsRequestTypeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddFriends extends AppCompatActivity {
    EditText friendname;
    Button searche;
    RecyclerView searcheFriendRecyclerView;
    RecyclerView friendsRequestRecyclerView;
    List<SearchResult> friendsList;
    List<FriendsRequestTypeInRV> requestList;
    FriendsRequestTypeAdapter requestAdapter;
    SearchResultAdapter searchResultAdapter;
    SharedPreferences sharedPreferences;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);
        actionBar=getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#253A4B")));
        actionBar.setTitle("Friends");
        actionBar.setDisplayHomeAsUpEnabled(true);
        friendname=(EditText) findViewById(R.id.addfriendedittext);
        searche=(Button)findViewById(R.id.searchbutton);
        searcheFriendRecyclerView =(RecyclerView)findViewById(R.id.friendsrv);
        friendsList=new ArrayList<>();
        searchResultAdapter =new SearchResultAdapter(friendsList, new SearchResultAdapter.ItemListener() {
            @Override
            public void onClick(View v, int position) {
                OnlineService.socket.emit("addfriends",sharedPreferences.getString("username",""), searchResultAdapter.getname(position));
                Button t=(Button)v;
                t.setText("requested");
            }
        });
        searcheFriendRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searcheFriendRecyclerView.setAdapter(searchResultAdapter);
        sharedPreferences=getSharedPreferences("myuser", Context.MODE_PRIVATE);
        OnlineService.socket.on("resultaddfriends", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONArray ja;
                        friendsList.clear();
                       // addFriendsAdapter=new
                        try {
                            ja=new JSONArray(args[0].toString());
                            for (int i=0;i<ja.length();i++){
                                JSONObject temp= ja.getJSONObject(i);
                                friendsList.add(new SearchResult(temp.getString("username")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        searchResultAdapter.updateAdapter(friendsList);
                    }
                });

            }
        });

        friendsRequestRecyclerView=(RecyclerView)findViewById(R.id.friends_request_rv);
        requestList=new ArrayList<>();
        requestAdapter=new FriendsRequestTypeAdapter(requestList, new FriendsRequestTypeAdapter.ItemListener() {
            @Override
            public void onAcceptClick(View v, int position) {
                OnlineService.socket.emit("acceptFriendsRequest",sharedPreferences.getString("username",""),requestAdapter.getname(position));
                requestList.remove(position);
                requestAdapter.updateAdapter(requestList);
            }

            @Override
            public void onDeleteClick(View v, int position) {
                OnlineService.socket.emit("deleteFriendsRequest",sharedPreferences.getString("username",""),requestAdapter.getname(position));
                requestList.remove(position);
                requestAdapter.updateAdapter(requestList);
            }
        });
        friendsRequestRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        friendsRequestRecyclerView.setAdapter(requestAdapter);
        OnlineService.socket.emit("myfriendsrequest",sharedPreferences.getString("username",""));
        OnlineService.socket.on("yourfriendsrequest", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONArray ja;
                        requestList.clear();
                        try {
                            ja=new JSONArray(args[0].toString());
                            for (int i=0;i<ja.length();i++){
                                JSONObject temp= ja.getJSONObject(i);
                                requestList.add(new FriendsRequestTypeInRV(temp.getString("friend_one")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        requestAdapter.updateAdapter(requestList);
                    }
                });
            }
        });
    }
    public void onSearcheClick(View v){
        OnlineService.socket.emit("searchefriends",sharedPreferences.getString("username",""),friendname.getText().toString());
        Toast.makeText(AddFriends.this,"done",Toast.LENGTH_LONG).show();
    }


    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent in=new Intent(AddFriends.this,MainActivity.class);
                startActivity(in);
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Intent in=new Intent(AddFriends.this,MainActivity.class);
            startActivity(in);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}